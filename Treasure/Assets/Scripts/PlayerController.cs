﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private enum Movement
    {
        NONE,
        RIGHT,
        LEFT
    }

    [SerializeField] private float _speed;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Rigidbody2D _rigidbody2D;

    private Movement _movement;

    private void Update()
    {
        _movement = Movement.NONE;
        if (Input.GetKey(KeyCode.RightArrow))
        {
            _movement = Movement.RIGHT;
            _spriteRenderer.flipX = true;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            _movement = Movement.LEFT;
            _spriteRenderer.flipX = false;
        }
    }

    private void FixedUpdate()
    {
        if (_movement == Movement.RIGHT)
            _rigidbody2D.position += Vector2.right * _speed * Time.deltaTime;
        if (_movement == Movement.LEFT)
            _rigidbody2D.position += Vector2.left * _speed * Time.deltaTime;
    }
}